import React from 'react';
import _ from 'lodash';

import ICCFile from './iccfile';
import ICCSubmit from './iccsubmit';
import ICCEntry from './iccentry';

/**
 * Home
 * Responsible for rendering ICC-interactive elements, and managing their states
 */
export default class Home extends React.Component {
  constructor() {
    super();
    this.state = {
      ICCFiles: [],
      ICCFile: {
        name: null,
        data: {
          version: null,
          intent: null,
          cmm: null,
          deviceClass: null,
          colorSpace: null,
          connectionSpace: null,
          platform: null,
          manufacturer: null,
          model: null,
          creator: null
        }
      }
    }
  }

  /**
   * Initializes the file-display after the component has mounted
   */
  componentDidMount() {
    var _this = this;
    console.log("Fetcing all files");
    fetch('/icc', {method: 'GET'})
      .then(function(response) {
      return response.json();
    }).then(function(json) {
      _this.setState({ICCFiles: json});
      console.log(json);
    });
  }

  /**
   * Retrieves the ICC information of a file when clicked
   * @param filename Name of the file to retrieve
   */
  getICC(filename) {
    var _this = this;
    console.log("Fetching a file");
    fetch('/icc/' + filename, {method: 'GET'})
      .then(function(response) {
        return response.json();
      }).then(function(json) {
      _this.setState({ICCFile: {
        name: filename,
        data: json
      }});
      console.log(json);
    });
  };

  /**
   * Removes a file from the store, and the corresponding elements from the DOM
   * @param filename Name of the file to delete
   */
  removeICC(filename) {
    var _this = this;
    console.log("Removing " + filename);

    fetch('/icc/' + filename, {method: 'DELETE'})
      .then(function() {
        _this.setState({
          ICCFiles: _.without(_this.state.ICCFiles, filename)
        });
      });
  };

  /**
   * Renders a submit form, list over stored files and information field for a selected file
   */
  render() {
    return(
      <div>
        <div className="col-sm-12">
          <ICCSubmit />
        </div>
        <div className="col-sm-4">
          <ul className="list-group">
            {this.state.ICCFiles.map( (name, key) =>
               <li className="list-group-item" key={key}><ICCFile name={name} removeICC={this.removeICC.bind(this)} getICC={this.getICC.bind(this)} /></li>
            )}
          </ul>
        </div>
        <div className="col-sm-6">
          <ICCEntry content={this.state.ICCFile} />
        </div>
      </div>
    )
  }
};