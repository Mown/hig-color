import React from 'react';

/**
 * ICCFile
 * Element of the file list
 */
export default React.createClass({
  render() {
    return(
      <div>
        <button className="glyphicon glyphicon-remove btn btn-xs btn-danger" aria-hidden="true" onClick={() => this.props.removeICC(this.props.name)} />  <u onClick={() => this.props.getICC(this.props.name)}>{this.props.name}</u>
      </div>
    )
  }
});