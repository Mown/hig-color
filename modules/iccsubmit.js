import React from 'react';

/**
 * ICCSubmit
 * Form element for uploading files
 */
export default React.createClass({
  render() {
    return(
      <div>
        <form encType="multipart/form-data" action="/icc" method="post">
          <input type="file" name="ICC" />
          <input type="submit" value="Upload ICC" name="submit" />
        </form>
      </div>
    )
  }
});