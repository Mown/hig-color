import React from 'react';

/**
 * Header
 * Simple header component using bootstrap css elements
 */
export default React.createClass({
  render() {
    return(
      <div className="page-header">
        <h1>ICC File Store</h1>
      </div>
    )
  }
});