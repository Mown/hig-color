import React from 'react';

/**
 * ICCEntry
 * Displays information of a selected ICC file
 */
export default React.createClass({
   render() {
    return(
      <div className="list-group">
        <p className="list-group-item active">Name: {this.props.content.name}</p>
        <p className="list-group-item">Version: {this.props.content.data.version}</p>
        <p className="list-group-item">Intent: {this.props.content.data.intent}</p>
        <p className="list-group-item">CMM: {this.props.content.data.cmm}</p>
        <p className="list-group-item">Device Class: {this.props.content.data.deviceClass}</p>
        <p className="list-group-item">Color Space: {this.props.content.data.colorSpace}</p>
        <p className="list-group-item">Connection Space: {this.props.content.data.connectionSpace}</p>
        <p className="list-group-item">Platform: {this.props.content.data.platform}</p>
        <p className="list-group-item">Manufacturer: {this.props.content.data.manufacturer}</p>
        <p className="list-group-item">Model: {this.props.content.data.model}</p>
        <p className="list-group-item">Creator: {this.props.content.data.creator}</p>
      </div>
    )
  }
});