import React from 'react';

import Header from './header';
import Home from './home';

/**
 * App
 * Component for primary UI elements
 */
export default React.createClass({
  render() {
    return(
      <div>
        <Header />
        <Home />
      </div>
    )
  }
});