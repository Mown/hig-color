var express = require('express');
var fs = require('fs');
var icc = require('icc');
var multer = require('multer');
var path = require('path');

var router = express.Router();

router.get('/', function(req, res) {
  fs.readdir('icc-store/', function(err, data) {
    if(err) console.log(err);
    res.send(data);
  })
});

var storage = multer.diskStorage({
  destination: function(req, file, callback) {
    callback(null, 'icc-store/');
  },
  filename: function(req, file, callback) {
    callback(null, file.originalname);
  }
});
var upload = multer({storage: storage}).single('ICC');

/**
 * Retrieves ICC information from a single item
 */
router.get('/:id', function(req, res) {
  fs.readFile('icc-store/' + req.params.id, function(err, data) {
    if(err) console.log(err);
    var profile = icc.parse(data);
    res.send(profile);
  })
});

/**
 * Stores files sent by the client in the icc-store/ folder
 */
router.post('/', function(req, res) {
  upload(req, res, function(err) {
    if(err) console.log(err);
  });
  res.sendFile(path.join(__dirname, '../public', 'index.html'));
});

/**
 * Removes an item from the database
 */
router.delete('/', function(req, res) {
  fs.unlink('icc-store/' + req.params.id, function(err) {
    if(err) console.log(err);
    res.send("Item deleted");
  })
});

module.exports = router;