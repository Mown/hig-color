import React from 'react'
import { render } from 'react-dom'
import {Router, Route, browserHistory, IndexRoute} from 'react-router';

import App from './modules/app';

/**
 * Main entry point, begins the rendering process of the app
 */
render((
  <App />
), document.getElementById('app'));