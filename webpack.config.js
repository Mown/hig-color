var webpack = require('webpack')

module.exports = {
  // Enabled for better code inspection
  devtool: 'eval',

  // Origin point for building our app
  entry: './index.js',

  // Bundle file is created in /public/bundle.js
  output: {
    path: 'public',
    filename: 'bundle.js',
    publicPath: '/'
  },

  // File loaders for javascript and json files
  module: {
    loaders: [
      { test: /\.js$/, exclude: /node_modules/, loader: 'babel-loader?presets[]=es2015&presets[]=react' },
      { test: /\.json$/, exclude: /node_modules/, loader: 'json-loader' }
    ]
  },

  // Enables the use of the fs library
  node: {
    fs: "empty"
  },

  // Optimization for a production environment
  plugins: process.env.NODE_ENV === 'production' ? [
    new webpack.optimize.DedupePlugin(),
    new webpack.optimize.OccurrenceOrderPlugin(),
    new webpack.optimize.UglifyJsPlugin()
  ] : []
}
